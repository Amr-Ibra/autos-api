package com.galvanize.autos;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AutosController.class)
public class AutosControllerTests {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private AutosService autosService;

  private final ObjectMapper mapper = new ObjectMapper();

  // GET: /api/autos response 200 when at least one vehicle exists in DB
  // returns all vehicles
  @Test
  void getAutos_dbHasContent_noParams_shouldReturnAllAutos() throws Exception {
    // Arrange
    final List<Automobile> automobiles = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      automobiles.add(new Automobile(1987 + i, "Volkswagen", "Golf", "XYZ" + i));
    }
    when(this.autosService.getAutos()).thenReturn(new AutosList(automobiles));
    // Act
    this.mockMvc.perform(get("/api/autos"))
        // (optional to print MvcResult in the console)
        .andDo(print())
        // Assert
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.automobiles", hasSize(5)));
  }

  // GET: /api/autos response 204 when no automobiles found in DB
  @Test
  void getAutos_dbIsEmpty_noParams_shouldReturnNoContent() throws Exception {
    // Arrange
    when(this.autosService.getAutos()).thenReturn(new AutosList());
    // Act
    this.mockMvc.perform(get("/api/autos"))
        .andDo(print())
        // Assert
        .andExpect(status().isNoContent());
  }

  // GET: /api/autos?color=white response 200 returns all white vehicles
  @Test
  void getAutos_searchByColor_shouldReturnSpecificAutos() throws Exception {
    // Arrange
    final List<Automobile> automobiles = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      automobiles.add(new Automobile(1987 + i, "Volkswagen", "Golf", "XYZ" + i));
    }
    when(this.autosService.getAutos(anyString())).thenReturn(new AutosList(automobiles));
    // Act
    this.mockMvc.perform(get("/api/autos?color=white"))
        // Assert
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.automobiles", hasSize(5)));
  }

  // GET: /api/autos?make=volkswagen response 200 returns all Volkswagen vehicles
  @Test
  void getAutos_searchByMake_shouldReturnSpecificAutos() throws Exception {
    // Arrange
    final List<Automobile> automobiles = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      automobiles.add(new Automobile(1987 + i, "Volkswagen", "Golf", "XYZ" + i));
    }
    when(this.autosService.getAutos(anyString())).thenReturn(new AutosList(automobiles));
    // Act
    this.mockMvc.perform(get("/api/autos?make=volkswagen"))
        // Assert
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.automobiles", hasSize(5)));
  }

  // GET: /api/autos?make=volkswagen&color=white response 200 returns all white
  // Volkswagen vehicles
  @Test
  void getAutos_searchByMakeAndColor_shouldReturnSpecificAutos() throws Exception {
    // Arrange
    final List<Automobile> automobiles = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      automobiles.add(new Automobile(1987 + i, "Volkswagen", "Golf", "XYZ" + i));
    }
    when(this.autosService.getAutos(anyString(), anyString())).thenReturn(new AutosList(automobiles));
    // Act
    this.mockMvc.perform(get("/api/autos?make=volkswagen&color=white"))
        // Assert
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.automobiles", hasSize(5)));
  }

  // GET: /api/autos/{vin} response 200 OK
  // returns the vehicle with the requested vin
  @Test
  void getAuto_withVin_existent_shouldReturnSpecificAuto() throws Exception {
    // Arrange
    final Automobile automobile = new Automobile(1987, "Volkswagen", "Golf", "XYZ");
    when(this.autosService.getAuto("XYZ")).thenReturn(automobile);
    // Act
    this.mockMvc.perform(get("/api/autos/" + automobile.getVin()))
        // Assert
        .andExpect(status().isOk())
        .andExpect(jsonPath("vin").value(automobile.getVin()));
  }

  // GET: /api/autos/{vin} response 204 when vehicle not found
  @Test
  void getAuto_withVin_nonexistent_shouldReturn204() throws Exception {
    // Arrange
    when(this.autosService.getAuto("XYZ")).thenReturn(null);
    // Act
    this.mockMvc.perform(get("/api/autos/xyz"))
        // Assert
        .andExpect(status().isNoContent());
  }

  // POST: /api/autos response 200 when automobile added successfully
  // returns the added vehicle
  @Test
  void addAuto_validRequest_shouldReturnAuto() throws Exception {
    // Arrange
    final Automobile automobile = new Automobile(1987, "Volkswagen", "Golf", "XYZ");
    when(this.autosService.addAuto(any(Automobile.class))).thenReturn(automobile);
    // Act
    this.mockMvc
        .perform(post("/api/autos").contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(automobile)))
        .andDo(print())
        // Assert
        .andExpect(status().isOk())
        .andExpect(jsonPath("make").value("Volkswagen"));
  }

  // POST: /api/autos response 400 bad request
  @Test
  void addAuto_badRequest_shouldReturn400() throws Exception {
    // Arrange
    final String json = "{\"year\":1987,\"make\":\"Volkswagen\",\"model\":\"Golf\",\"color\":null,\"owner\":null}";
    when(this.autosService.addAuto(any(Automobile.class))).thenThrow(InvalidAutoException.class);
    // Act
    this.mockMvc
        .perform(post("/api/autos").contentType(MediaType.APPLICATION_JSON).content(json))
        .andDo(print())
        // Assert
        .andExpect(status().isBadRequest());
  }

  // PATCH: /api/autos/{vin} response 200 when vehicle updated successfully
  // returns updated vehicle
  @Test
  void updateAuto_withObject_shouldReturnAuto() throws Exception {
    // Arrange
    final Automobile automobile = new Automobile(1987, "Volkswagen", "Golf", "XYZ");

    automobile.setColor("red");
    automobile.setOwner("Max");

    when(this.autosService.updateAuto(anyString(), anyString(), anyString())).thenReturn(automobile);
    // Act
    this.mockMvc.perform(patch("/api/autos/" + automobile.getVin())
        .contentType(MediaType.APPLICATION_JSON).content("{\"color\":\"red\",\"owner\":\"Max\"}"))
        // Assert
        .andExpect(status().isOk())
        .andExpect(jsonPath("color").value("red")).andExpect(jsonPath("owner").value("Max"));
  }

  // PATCH: /api/autos/{vin} response 204 when vehicle not found
  @Test
  void updateAuto_nonexistent_shouldReturn204() throws Exception {
    // Arrange
    when(this.autosService.updateAuto(anyString(), anyString(), anyString())).thenReturn(null);
    // Act
    this.mockMvc.perform(patch("/api/autos/xyz")
        .contentType(MediaType.APPLICATION_JSON).content("{\"color\":\"red\",\"owner\":\"Max\"}"))
        // Assert
        .andExpect(status().isNoContent());
  }

  // PATCH: /api/autos/{vin} response 400 when bad request
  @Test
  void updateAuto_badRequest_shouldReturn400() throws Exception {
    // Arrange
    when(this.autosService.updateAuto(anyString(), anyString(), anyString())).thenThrow(new InvalidAutoException());
    // Act
    this.mockMvc.perform(patch("/api/autos/xyz"))
        .andExpect(status().isBadRequest());
  }

  // DELETE: /api/autos/{vin} response 202 Automobile delete request accepted
  @Test
  void deleteAuto_withVin_existent_shouldReturn202() throws Exception {
    // Act
    this.mockMvc.perform(delete("/api/autos/xyz"))
        // Assert
        .andExpect(status().isAccepted());
    verify(this.autosService).deleteAuto(anyString());
  }

  // DELETE: /api/autos/{vin} response 204 vehicle not found
  @Test
  void deleteAuto_withVin_nonexistent_shouldReturn204() throws Exception {
    doThrow(new AutoNotFoundException()).when(this.autosService).deleteAuto(anyString());
    // Act
    this.mockMvc.perform(delete("/api/autos/xyz"))
        // Assert
        .andExpect(status().isNoContent());
  }

}
