package com.galvanize.autos;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
class AutosApiApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AutosRepository autosRepository;

    private final Random random = new Random();
    private List<Automobile> testAutos;

    @BeforeEach
    void setUp() {
        this.testAutos = new ArrayList<>();
        Automobile auto;
        final String[] colors = { "red", "blue", "green", "orange", "yellow",
                "black", "brown", "root beer", "magneta", "amber" };

        for (int i = 0; i < 50; i++) {
            if ((i % 3) == 0) {
                auto = new Automobile(1987, "Volkswagen", "Golf", "GGIIJJ" + (i * 13));
                auto.setColor(colors[this.random.nextInt(10)]);
            } else if ((i % 2) == 0) {
                auto = new Automobile(2000, "Dodge", "Viper", "AABBCC" + (i * 12));
                auto.setColor(colors[this.random.nextInt(10)]);
            } else {
                auto = new Automobile(2020, "Audi", "A8", "DDEEFF" + (i * 11));
                auto.setColor(colors[this.random.nextInt(10)]);
            }
            this.testAutos.add(auto);
        }

        this.autosRepository.saveAll(this.testAutos);
    }

    @AfterEach
    void tearDown() {
        this.autosRepository.deleteAll();
    }

    @Test
    void contextLoads() {
    }

    @Test
    void getAutos_dbHasContent_noParams_shouldReturnAutosList() {
        final ResponseEntity<AutosList> response = this.restTemplate.getForEntity("/api/autos", AutosList.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();

        // for demo purposes
        for (final Automobile auto : response.getBody().getAutomobiles()) {
            System.out.println(auto);
        }
    }

    @Test
    void getAutos_searchByMakeAndColor_shouldReturnSpecificAutos() {
        final int seq = this.random.nextInt(50);
        final String color = this.testAutos.get(seq).getColor();
        final String make = this.testAutos.get(seq).getMake();

        final ResponseEntity<AutosList> response = this.restTemplate
                .getForEntity(String.format("/api/autos?make=%s&color=%s", make, color), AutosList.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().isEmpty()).isFalse();
        assertThat(response.getBody().getAutomobiles().size()).isGreaterThanOrEqualTo(1);

        // for demo purposes
        for (final Automobile auto : response.getBody().getAutomobiles()) {
            System.out.println(auto);
        }
    }

    @Test
    void addAuto_validRequest_shouldReurnAuto() {
        // Arrange
        final Automobile automobile = new Automobile(1987, "Volkswagen", "Golf", "XYZ03");
        automobile.setColor("red");

        final HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        final HttpEntity<Automobile> request = new HttpEntity<>(automobile, headers);

        // Act
        final ResponseEntity<Automobile> response = this.restTemplate.postForEntity("/api/autos", request,
                Automobile.class);

        // Assert
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getVin()).isEqualTo(automobile.getVin());
    }

}
