package com.galvanize.autos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AutosServiceTests {

    private AutosService autosService;
    private Automobile testAuto;

    @Mock
    private AutosRepository autosRepository;

    @BeforeEach
    void init() {
        this.autosService = new AutosService(this.autosRepository);
        this.testAuto = new Automobile(1987, "Volkswagen", "Golf", "XYZ");
        this.testAuto.setColor("red");
    }

    @Test
    void getAutos_noParams_shouldReturnAllAutos() {
        when(this.autosRepository.findAll()).thenReturn(Arrays.asList(this.testAuto));

        final AutosList autosList = this.autosService.getAutos();
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void getAutos_searchByMakeAndColor_shouldReturnSpecificAutos() {
        when(this.autosRepository.findByMakeAndColor(ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString()))
                        .thenReturn(Arrays.asList(this.testAuto));

        final AutosList autosList = this.autosService.getAutos("Volkswagen", "red");
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void getAutos_searchByMake_shouldReturnSpecificAutos() {
        when(this.autosRepository.findByMake(ArgumentMatchers.anyString()))
                .thenReturn(Arrays.asList(this.testAuto));

        final AutosList autosList = this.autosService.getAutos("Volkswagen");
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void getAutos_searchByColor_shouldReturnSpecificAutos() {
        when(this.autosRepository.findByColor(ArgumentMatchers.anyString()))
                .thenReturn(Arrays.asList(this.testAuto));

        final AutosList autosList = this.autosService.getAutos("red");
        assertThat(autosList).isNotNull();
        assertThat(autosList.isEmpty()).isFalse();
    }

    @Test
    void getAuto_withVin_shouldReturnAuto() {
        when(this.autosRepository.findByVin(ArgumentMatchers.anyString()))
                .thenReturn(Optional.of(this.testAuto));

        final Automobile auto = this.autosService.getAuto(this.testAuto.getVin());
        assertThat(auto).isNotNull();
        assertThat(auto.getVin()).isEqualTo(this.testAuto.getVin());
    }

    @Test
    void addAuto_validRequest_shouldReturnAuto() {
        when(this.autosRepository.save(ArgumentMatchers.any(Automobile.class)))
                .thenReturn(this.testAuto);

        final Automobile auto = this.autosService.addAuto(this.testAuto);
        assertThat(auto).isNotNull();
        assertThat(auto.getMake()).isEqualTo("Volkswagen");
    }

    @Test
    void addAuto_badRequest() {
        when(this.autosRepository.save(ArgumentMatchers.any(Automobile.class)))
                .thenThrow(new InvalidAutoException());

        assertThatThrownBy(() -> this.autosService.addAuto(this.testAuto))
                .isInstanceOf(InvalidAutoException.class);
    }

    @Test
    void updateAuto_shouldReturnAuto() {
        when(this.autosRepository.findByVin(ArgumentMatchers.anyString()))
                .thenReturn(Optional.of(this.testAuto));
        when(this.autosRepository.save(ArgumentMatchers.any(Automobile.class)))
                .thenReturn(this.testAuto);

        final Automobile auto = this.autosService.updateAuto(this.testAuto.getVin(), "white", "Max");
        assertThat(auto).isNotNull();
        assertThat(auto.getVin()).isEqualTo(this.testAuto.getVin());
    }

    @Test
    void deleteAuto_byVin_existent() {
        when(this.autosRepository.findByVin(ArgumentMatchers.anyString()))
                .thenReturn(Optional.of(this.testAuto));

        this.autosService.deleteAuto(this.testAuto.getVin());

        verify(this.autosRepository).delete(this.testAuto);
    }

    @Test
    void deleteAuto_byVin_nonexistent() {
        when(this.autosRepository.findByVin(ArgumentMatchers.anyString()))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> this.autosService.deleteAuto("nonexistent")).isInstanceOf(AutoNotFoundException.class);
    }

}
