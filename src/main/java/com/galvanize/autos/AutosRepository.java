package com.galvanize.autos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AutosRepository extends JpaRepository<Automobile, Long> {

    List<Automobile> findByMakeAndColor(String make, String color);

    List<Automobile> findByMake(String make);

    List<Automobile> findByColor(String color);

    Optional<Automobile> findByVin(String vin);

}
