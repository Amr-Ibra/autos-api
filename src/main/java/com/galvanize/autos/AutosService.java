package com.galvanize.autos;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
public class AutosService {

    private final AutosRepository autosRepository;

    public AutosService(AutosRepository autosRepository) {
        this.autosRepository = autosRepository;
    }

    public AutosList getAutos() {
        // Query: select * from autos
        // Put that in a list
        // Return a new AutosList with the list of autos
        return new AutosList(this.autosRepository.findAll());
    }

    public AutosList getAutos(String make, String color) {
        final List<Automobile> automobiles = this.autosRepository.findByMakeAndColor(make, color);
        if (!automobiles.isEmpty()) {
            return new AutosList(automobiles);
        }
        return null;
    }

    public AutosList getAutos(String makeOrcolor) {
        final List<Automobile> automobilesByMake = this.autosRepository.findByMake(makeOrcolor);
        final List<Automobile> automobilesByColor = this.autosRepository.findByColor(makeOrcolor);

        if (!automobilesByMake.isEmpty()) {
            return new AutosList(automobilesByMake);
        } else if (!automobilesByColor.isEmpty()) {
            return new AutosList(automobilesByColor);
        }

        return null;
    }

    public Automobile getAuto(String vin) {
        return this.autosRepository.findByVin(vin).orElse(null);
    }

    public Automobile addAuto(Automobile automobile) {
        return this.autosRepository.save(automobile);
    }

    public Automobile updateAuto(String vin, String color, String owner) {
        final Optional<Automobile> oAuto = this.autosRepository.findByVin(vin);
        if (oAuto.isEmpty()) {
            return null;
        } else {
            oAuto.get().setColor(color);
            oAuto.get().setOwner(owner);
            return this.autosRepository.save(oAuto.get());
        }
    }

    public void deleteAuto(String vin) {
        final Optional<Automobile> oAuto = this.autosRepository.findByVin(vin);
        if (oAuto.isPresent()) {
            this.autosRepository.delete(oAuto.get());
        } else {
            throw new AutoNotFoundException();
        }
    }

}
