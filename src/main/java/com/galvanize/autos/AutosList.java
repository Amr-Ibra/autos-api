package com.galvanize.autos;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AutosList {

    private List<Automobile> automobiles;

    public AutosList() {
        this.setAutomobiles(new ArrayList<>());
    }

    public AutosList(List<Automobile> automobiles) {
        this.setAutomobiles(automobiles);
    }

    public List<Automobile> getAutomobiles() {
        return this.automobiles;
    }

    public void setAutomobiles(List<Automobile> automobiles) {
        this.automobiles = automobiles;
    }

    @JsonIgnore
    public boolean isEmpty() {
        return this.automobiles.isEmpty();
    }

}
