package com.galvanize.autos;

public class UpdateAutoRequest {
  private String color;
  private String owner;

  public String getColor() {
    return this.color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getOwner() {
    return this.owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

}
