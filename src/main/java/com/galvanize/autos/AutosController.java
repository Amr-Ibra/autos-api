package com.galvanize.autos;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AutosController {

  private final AutosService autosService;

  public AutosController(AutosService autosService) {
    this.autosService = autosService;
  }

  @GetMapping("/api/autos")
  public ResponseEntity<AutosList> getAutos(@RequestParam(required = false) String make,
      @RequestParam(required = false) String color) {

    final AutosList autosList;

    if ((make == null) && (color == null)) {
      autosList = this.autosService.getAutos();
    } else if (make == null) {
      autosList = this.autosService.getAutos(color);
    } else if (color == null) {
      autosList = this.autosService.getAutos(make);
    } else {
      autosList = this.autosService.getAutos(make, color);
    }

    return autosList.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(autosList);
  }

  @GetMapping("/api/autos/{vin}")
  public ResponseEntity<Automobile> getAuto(@PathVariable String vin) {
    final Automobile automobile = this.autosService.getAuto(vin);
    return automobile == null ? ResponseEntity.noContent().build() : ResponseEntity.ok(automobile);
  }

  @PostMapping("/api/autos")
  public Automobile addAuto(@RequestBody Automobile auto) {
    return this.autosService.addAuto(auto);
  }

  @PatchMapping("/api/autos/{vin}")
  public ResponseEntity<Automobile> updateAuto(@PathVariable String vin, @RequestBody UpdateAutoRequest update) {
    final Automobile automobile = this.autosService.updateAuto(vin, update.getColor(), update.getOwner());
    return automobile == null ? ResponseEntity.noContent().build() : ResponseEntity.ok(automobile);
  }

  @DeleteMapping("/api/autos/{vin}")
  public ResponseEntity<Automobile> deleteAuto(@PathVariable String vin) {
    try {
      this.autosService.deleteAuto(vin);
    } catch (final AutoNotFoundException e) {
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.accepted().build();
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public void handleInvalidAutoException(InvalidAutoException e) {
    // TODO Auto-generated method stub
  }

}
